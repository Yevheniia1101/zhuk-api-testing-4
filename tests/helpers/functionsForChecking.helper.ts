import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}

export function checkUserName(response, userName) {
    expect(response.body.user.userName, 'User Name is').to.be.equal(userName);
}

export function checkUpdatedUserName(response, userName) {
    expect(response.body.userName, 'User Name is').to.be.equal(userName);
}


export function getMyPostByID(response, postId:number) {
let i:number;
for ( i = 0; i < response.body.length;i++) {
if (response.body[i].id === postId)
console.log('my post info', response.body[i]);
};
}