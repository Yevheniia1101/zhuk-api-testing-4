import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();


describe(`Negative tests. `, () => {
    let accessToken: string;
    let userId: number;
     

    before (`Registration`, async () => {
       
        let response = await users.createNewUser("string", 'mytest20@test.com', 'jenya4', 'test');
         userId = response.body.user.id;  
            accessToken = response.body.token.accessToken.token;
                }); 

        after (`Delete current user by ID`, async () => {
         let response = await users.deleteUserById(userId, accessToken);
         
        }); 

        it(`1 - NEGATIVE Login with invalid email`, async () => {
            let response = await auth.login("mtest8@test.com", "test");
            checkStatusCode(response, 404);
            checkResponseTime(response,500);
            console.log('Status Code for invalid login is ', response.statusCode)
            console.log('Time for invalid login is ', response.timings.phases.total)

        });

         
        it(`2 - NEGATIVE Update User with invalid data`, async () => {
            let userData: object = {
                id: 100000000000,
                avatar: "string",
                email: "mytest20@test.com",
                userName: "jenya22",
            };
    
            let response = await users.updateUser(userData, accessToken);
            checkStatusCode(response, 400);
            checkResponseTime(response,500);
            console.log('Status Code for update user with invalid  is ', response.statusCode)
            console.log('Time for update user with invalid  is ', response.timings.phases.total)

        }); 

        it(`3 - NEGATIVE Get the data of the currect user by invalid token`, async () => {
            let response = await users.getCurrentUser('bla-bla-bla');
            checkStatusCode(response, 401);
            checkResponseTime(response,500);
            console.log('Status Code for invalid token is ', response.statusCode)
            console.log('Time for invalid token is ', response.timings.phases.total)
        });

       
    
    });

    
        
    

   