import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime, getMyPostByID } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const posts = new PostsController();

describe(`Post tests.`, () => {
    
    let accessToken: string;
    let userId: number;
    let postId: number;
    let commentId: number;

    before (`Registration in the system`, async () => {
        let response = await users.createNewUser("string", 'mytest32@test.com', 'jenya4', 'test');
        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
        
         });

         after (`Delete current user by ID`, async () => {
            let response = await users.deleteUserById(userId, accessToken);
            
        });

        it (`1 - Add a new post`, async () => {
            let postData: object = {
                id: userId,
                avatar: "string",
                previewImage: "string",
                body: "My new post"
            };

            let response = await posts.createNewPost(postData, accessToken);
            postId = response.body.id;
            checkStatusCode(response, 200);
            checkResponseTime(response,500);
            expect(response.body.body, 'New Post is').to.be.equal('My new post');

            console.log('Status Code for add new post is ', response.statusCode)
            console.log('Time for add new post is ', response.timings.phases.total)
            console.log('post ID', postId);
        });  

            it (`2 - Display all posts`, async () => {
            let response = await posts.getAllPosts();
            checkStatusCode(response, 200);
            checkResponseTime(response,1500);
            expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);

            console.log('Status Code for display all posts is ', response.statusCode)
            console.log('Time for display all posts is ', response.timings.phases.total)
            console.log('Response body consists of ', response.body.length, 'items')
        }); 

         it (`3 - Add Like to my Post`, async () => {
         let likeData: object = {
                entityId: postId,
                isLike: true,
                id: userId};

            let response = await posts.addLikeReaction(likeData, accessToken);
            checkStatusCode(response, 200);
            checkResponseTime(response,500);
            
            console.log('Status Code for add Like to Post is ', response.statusCode);
            console.log('Time for add Like to Post is ', response.timings.phases.total)
            
         });
               
        it (`4 - Add a comment to my post by ID`, async () => {
            let commentData: object = {
                authorId: userId,
                postId: postId,
                body: 'The first comment to this Post'};
            let response = await posts.addCommentToPost(commentData, accessToken);
            commentId = response.body.id;          
            
            checkStatusCode(response, 200);
            checkResponseTime(response,500);
            
            console.log('my comment ID is', commentId); 
            console.log('Status Code for add a comment to Post is ', response.statusCode);
            console.log('Time for add a comment to Post is ', response.timings.phases.total)

            });

            it (`5 - Display my post`, async () => {
                let response = await posts.getAllPosts();
                
                console.log('my post ID is ', postId);
                getMyPostByID(response, postId);
                                
                   });
             
    
                   
})   