import {    checkResponseTime, checkStatusCode} from '../../helpers/functionsForChecking.helper';
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from '../lib/controllers/auth.controller';

const auth = new AuthController();
const users = new UsersController();

describe('Use test data set for login', () => {
    let accessToken: string;
    let userId: number;
    let invalidCredentialsDataSet = [

        { email: 'mytest50@test.com', password: '' },          
        { email: 'mytest50@test.com', password: '      ' },    
        { email: 'mytest50@test.com', password: 'test!' },
        { email: 'mytest50@test.com', password: 'te st' },
        { email: 'mytest50@test.com', password: 'admin' },
        { email: 'mytest50@test.com', password: 'mytest50@test.com' },
    ];

    
    before (`Registration in the system`, async () => {
        let response = await users.createNewUser("string", 'mytest50@test.com', 'jenya4', 'test');
        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
        console.log('User ID', userId); 
         });


         after (`Delete current user by ID`, async () => {
            let response = await users.deleteUserById(userId, accessToken);
            
        });
         
        
    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });

        
    });
});

